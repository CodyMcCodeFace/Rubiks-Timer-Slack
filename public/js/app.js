(function(){
  'use strict';

  angular.module('rubiksApp', ['ngRoute'])
    .config(function($routeProvider){
        $routeProvider.otherwise({
          redirectTo: '/'
        });
    });
})();
